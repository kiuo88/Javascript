//imgData är en Array som innehåller alla pixelvärden som finns i bilden image.JPG (som laddas på sidan med <img>-taggen ovan)
//OBS! Fungerar korrekt bara med grayscale-bilder

function image1() {
	var imgData = getImageData("testbild1");
	showChart("chart1", imgData);
}
function image2() {
	var imgData = getImageData("testbild2");
	showChart("chart2", imgData);
}



function getImageData(imageId) {
	var img = document.getElementById(imageId);

	var c = document.createElement("canvas");
	c.width = img.width;
	c.height = img.height;
	c.setAttribute("hidden", true);

	var ctx = c.getContext("2d");
	ctx.drawImage(img, 0, 0);

	var imgData = ctx.getImageData(0, 0, c.width, c.height).data;
	return imgData;
};

function showChart(chartId, imgData) {

	var chart = "#" + chartId;
	//chart sizes defined
	var width = 1500;
	var height = 650;
	var margin = 75;
	var chartWidth = width - margin * 2;
	var chartHeight = height - margin * 2;
	var barWidth = 2;
	var barPadding = 1;

	//value handling
	var categories = [];
	var frequencies = [];
	var categoryMax = imgData.reduce(function (a, b) { return Math.max(a, b) });
	var categoryMin = imgData.reduce(function (a, b) { return Math.min(a, b) });
	//grayscale image is always 0-255
	var categoryStart = 0;
	var categoryEnd = 255;
	//every "color" needs to be represented
	var categoryInterval = 1;
	//if for some reason category interval would be anything else than 1, this is needed:
	var categoryCount = ((categoryEnd + 1) - categoryStart) / categoryInterval;
	var currentValue = categoryStart;
	for (var i = 0; i < categoryCount; i++) {
		var frequency = 0;
		categories.push(currentValue.toString() + "-" + (currentValue + categoryInterval - 1).toString());
		for (var j = 0; j < imgData.length; j += 4) {
			if (imgData[j] >= currentValue && imgData[j] < currentValue + categoryInterval)
				frequency++;
		}
		frequencies.push(frequency);
		currentValue += categoryInterval;
	}

	//defining scales
	var yScale = d3.scaleLinear()
		.domain([0, d3.max(frequencies)])
		.range([chartHeight, 0]);

	var yAxis = d3.axisLeft(yScale);

	var xScale = d3.scaleBand()
		.domain(categories)
		.range([0, (barWidth + barPadding) * categories.length - barPadding]);

	var xAxis = d3.axisBottom(xScale)
	.tickValues(xScale.domain().filter(function(d,i){ return !(i%10)}));

	var barScale = d3.scaleLinear()
		.domain([0, d3.max(frequencies)])
		.range([0, chartHeight]);
	//selecting our canvas
	var canvas = d3.select(chart)
		.append("svg")
		.attr("width", width)
		.attr("height", height);
	//starting the painting
	var chartGroup = canvas.append("g")
		.attr("transform", "translate(" + margin + "," + margin + ")");

	chartGroup.selectAll("rectangels")
		.data(frequencies)
		.enter()
		.append("rect")
		.attr("width", barWidth)
		.attr("height", function (data) { return barScale(data); })
		.attr("x", function (data, i) { return i * (barWidth + barPadding); })
		.attr("y", function (data) { return chartHeight - barScale(data); });

	chartGroup.append("g")
		.call(yAxis);

	chartGroup.append("g")
		.attr("transform", "translate(0, " + chartHeight + ")")
		.call(xAxis)
		.selectAll("text")
		.attr("x", 9)
		.attr("y", 0)
		.attr("transform", "rotate(90)")
		.style("font-size", "15px")
		.style("text-anchor", "start");
}