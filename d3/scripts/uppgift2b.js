// set the dimensions and margins of the graph
var margin = { top: 20, right: 20, bottom: 30, left: 50 },
    width = 960 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;

// set the ranges
var x = d3.scaleTime().range([0, width]);
var y = d3.scaleLinear().range([height, 0]);

// define the line
var line = d3.line()
    .x(function (d) { return x(d.date); })
    .y(function (d) { return y(d.rate); });
// define the line
//var GBPline = d3.line()
//    .x(function (d) { return x(d.date); })
//    .y(function (d) { return y(d.GBP); });
// define the line
//var JPYline = d3.line()
//    .x(function (d) { return x(d.date); } })
//    .y(function (d) { return y(d.JPY); } });
// define the line
//var SEKline = d3.line()
//    .x(function (d) { return x(d.date); } })
//    .y(function (d) {  return y(d.SEK); } });

// select the div graph element and append svg to it
// appends a 'group' element to 'svg'
// moves the 'group' element to the top left margin
var svg = d3.select("#graph").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform",
    "translate(" + margin.left + "," + margin.top + ")");

function draw(data) {

    //var data = data[country];

    // format the data
    data.forEach(function (d) {
        var format = d3.timeParse("%d-%m-%Y");
        d.date = format(d.date);
        d.rate = +d.rate;
        console.log("full range: " + d.date + " - " + d.rate);
    });

    // sort years ascending(not needed as the arrays are built up in a date order)
    /*data.sort(function (a, b) {
          return a["Date"] - b["Date"];
      })*/

    // Scale the range of the data
    x.domain(d3.extent(data, function (d) { return d.date; }));
    y.domain([0, d3.max(data, function (d) {
        return Math.max(d.rate);
    })]);

    // Nest the entries by symbol
    var dataNest = d3.nest()
        .key(function (d) { return d.currency; })
        .entries(data);

    dataNest.forEach(function (d, i) {
        // Add the USD path.
        svg.append("path")
            .attr("class", "line")
            .attr("d", line(d.values));

    });
    // Add the GBP path.
    /*        svg.append("path")
                .data([data])
                .attr("class", "line")
                .attr("d", GBPline);
        // Add the JPY path.
            svg.append("path")
                .data([data])
                .attr("class", "line")
                .attr("d", JPYline);
        // Add the SEK path.
            svg.append("path")
                .data([data])
                .attr("class", "line")
                .attr("d", SEKline);*/

    // Add the X Axis
    svg.append("g")
            .attr("transform", "translate(0," + height + ")")
            .call(d3.axisBottom(x));

    // Add the Y Axis
    svg.append("g")
        .call(d3.axisLeft(y));
}
// Get the data
//d3.json("data.json", function (error, data) {
//    if (error) throw error;

    // trigger render
//    draw(data, "Afghanistan");
//});
