var checkArray = [];
var tempJsonArray = [];
var jsonArray = [];

//selecting the target for our paintings:
var svg = d3.select("svg"),
    width = +svg.attr("width"),
    height = +svg.attr("height");

var simulation = d3.forceSimulation()
    .force("center", d3.forceCenter(width / 2, height / 2))
    .force("charge", d3.forceManyBody()
                        .strength(-666))
    .force("link", d3.forceLink());
//let's get the json and start the main d3 thingie
d3.json("facebookFriends.json", function (error, graph) {
    if (error) throw error;

    var namesArray = [];
    var handledNamesArray = [];
    var linksArray = [];
    var jsonishValue = "";
    var tempHandledName = "";
    //let's get the names/nodes array:
    for (i = 0; i < graph.facebookFriends.length; i++) {
        namesArray.push(graph.facebookFriends[i].name);
        tempHandledName = {
            id: graph.facebookFriends[i].name,
            group: 1
        };
        handledNamesArray.push(tempHandledName);

    }
    //let's get the info needed for links array:
    for (i = 0; i < graph.facebookFriends.length; i++) {
        for (j = 0; j < graph.facebookFriends[i].friends.length; j++) {
            var id = (graph.facebookFriends[i].friends[j].FriendId - 1);
            var link = (i + " " + id);
            linksArray.push(link);
        }
    }
    //let's build the links array:
    for (i = 0; i < linksArray.length; i++) {
        var tempArray = linksArray[i].split(" ")
        var tempValue = tempArray[1] + " " + tempArray[0];
        needle = tempValue;
        index = contains.call(checkArray, needle);
        if (index == false) {
            jsonishValue = {
                source: tempArray[0],
                target: tempArray[1],
                value: 1
            };
            jsonArray.push(jsonishValue);
            checkArray.push(linksArray[i]);
        }
        if (i == (linksArray.length - 1)) {
            checkArray = [];
        }
    }
    //let's start painting with d3
    var link = svg.append("g")
        .attr("class", "links")
        .selectAll("line")
        .data(jsonArray)
        .enter().append("line")
        .attr("stroke", "black");

    simulation
        .nodes(handledNamesArray);

    simulation
        .force("link")
        .links(jsonArray);

    var node = svg.selectAll(".node")
        .data(handledNamesArray)
        .enter().append("g")
        .attr("class", "node");

    node.append("circle")
        .attr("r", 6)
        .attr("fill", "black")
        .call(d3.drag()
        .on("start", dragstarted)
        .on("drag", dragged)
        .on("end", dragended));

    node.append("text")
        .attr("dx", 12)
        .attr("dy", ".35em")
        .text(function (d) { return d.id })
        .style("font-size", "15px");

    simulation.on("tick", function () {
        link.attr("x1", function (d) { return d.source.x; })
            .attr("y1", function (d) { return d.source.y; })
            .attr("x2", function (d) { return d.target.x; })
            .attr("y2", function (d) { return d.target.y; });

        node.attr("transform", function (d) { return "translate(" + d.x + "," + d.y + ")"; });
    });
});

//things for dragging the nodes around:
function dragstarted(d) {
    if (!d3.event.active) simulation.alphaTarget(0.3).restart();
    d.fx = d.x;
    d.fy = d.y;
}

function dragged(d) {
    d.fx = d3.event.x;
    d.fy = d3.event.y;
}

function dragended(d) {
    if (!d3.event.active) simulation.alphaTarget(0);
    d.fx = null;
    d.fy = null;
}



//the duplicate finder function:
var contains = function (needle) {
    // Per spec, the way to identify NaN is that it is not equal to itself
    var findNaN = needle !== needle;
    var indexOf;

    if (!findNaN && typeof Array.prototype.indexOf === 'function') {
        indexOf = Array.prototype.indexOf;
    } else {
        indexOf = function (needle) {
            var i = -1, index = -1;

            for (i = 0; i < this.length; i++) {
                var item = this[i];

                if ((findNaN && item !== item) || item === needle) {
                    index = i;
                    break;
                }
            }

            return index;
        };
    }

    return indexOf.call(this, needle) > -1;
};