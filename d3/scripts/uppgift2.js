//let's declare some global variables
var sDValue = [];
var eDValue = [];
var ratesFullRange = [];
var ratesObject = {
    currency: "",
    date: "",
    rate: ""
}
var amountOfRates = "";
var startDate = "";
var endDate = "";
//get start date
function startDateFunction() {
    var startDateValue = $("#startDate").val();
    if (startDateValue == '') {
        alert("Enter Some Text In Input Field");
    } else {
        var shortStartDate = convertDate(startDateValue);
        sDValue = shortStartDate.split(" ");
        if (sDValue[0].length == 1) {
            sDValue[0] = "0" + sDValue[0];
        }
        if (sDValue[1].length == 1) {
            sDValue[1] = "0" + sDValue[1];
        }
        document.getElementById("startDateSet").innerHTML = sDValue[0] + " " + sDValue[1] + " " + sDValue[2];
        startDate = new Date(sDValue[2] + "-" + sDValue[1] + "-" + sDValue[0]);
    }
}

//get end date and check that it's not before start date
function endDateFunction() {
    var endDateValue = $("#endDate").val();
    var testSDate = new Date($("#startDate").val());
    var testEDate = new Date($("#endDate").val());
    if (endDateValue == '') {
        alert("Enter Some Text In Input Field");
    }
    if (testSDate > testEDate) {
        alert("End date was before start date")
    }
    else {
        var shortEndDate = convertDate(endDateValue);
        eDValue = shortEndDate.split(" ");
        if (eDValue[0].length == 1) {
            eDValue[0] = "0" + eDValue[0];
        }
        if (eDValue[1].length == 1) {
            eDValue[1] = "0" + eDValue[1];
        }
        document.getElementById("endDateSet").innerHTML = eDValue[0] + " " + eDValue[1] + " " + eDValue[2];
        endDate = new Date(eDValue[2] + "-" + eDValue[1] + "-" + eDValue[0]);
    }
}
//converts long date into short date format
function convertDate(oldcDate) {
    var convertedOldDate = new Date(oldcDate);
    var month = convertedOldDate.getMonth() + 1;
    var date = convertedOldDate.getDate();
    var year = convertedOldDate.getFullYear();
    var shortcDate = date + " " + month + " " + year;
    return shortcDate;
}
//main meat of the site
function currencies() {
    $('#currencyButton').attr("disabled", true);
    var tempCheckedValue = [];
    var checkedValue = [];
    $('input[type=checkbox]').each(function () {
        tempCheckedValue.push(this.checked ? $(this).val() : "");
    });
    checkedValue = cleanArray(tempCheckedValue);
    var currencyForm = document.getElementById("#currencyForm");
    //did you check any checkboxes ?
    if (checkedValue.length == 0) {
        alert("You didn\'t choose any of the checkboxes!");
        $('#currencyButton').attr("disabled", false);
        return false;
        //did you choose a start date?
    } if (typeof sDValue[0] == 'undefined') {
        alert("You forgot to select a start date");
        $('#currencyButton').attr("disabled", false);
        return false;
        //did you choose an end date?
    } if (typeof eDValue[0] == 'undefined') {
        alert("You forgot to select a end date");
        $('#currencyButton').attr("disabled", false);
        return false;
        //everythings ready to move forward!
    } else {
        var currentDate = startDate;
        var currentArrayDate = startDate;
        var currentParsedDate = convertDashDate(currentDate);
        var arrayReadyDate = arrayedDate(currentArrayDate);
        console.log(currentParsedDate);
        console.log(arrayReadyDate);
        endDate = addDays(endDate, 1);
        var endParsedDate = convertDashDate(endDate);
        //let's get from start to end date
        while (currentParsedDate != endParsedDate) {
            var jsonCDate = "http://api.fixer.io/" + currentParsedDate;
            var tempCheckValue = 0;
            console.log("(array ready format)While loop: " + arrayReadyDate);
            $.getJSON(jsonCDate, function(data) {
                //let's get the correct currencies into nice arrays
                for (i = 0; i < checkedValue.length; i++) {
                    var tempValue = checkedValue[i];
                    var rate = data.rates[tempValue];
                    console.log(tempValue);
                    console.log("(array ready format)For loop inside getJSON: " + arrayReadyDate);
                    switch (tempValue) {
                        case "USD":
                            ratesObject = {
                                currency: tempValue,
                                date: arrayReadyDate,
                                rate: rate
                            }
                            ratesFullRange.push(ratesObject);
                            tempCheckValue++;
                            break;
                        case "GBP":
                            ratesObject = {
                                currency: tempValue,
                                date: arrayReadyDate,
                                rate: rate
                            }
                            ratesFullRange.push(ratesObject);
                            tempCheckValue++;
                            break;
                        case "JPY":
                            ratesObject = {
                                currency: tempValue,
                                date: arrayReadyDate,
                                rate: rate
                            }
                            ratesFullRange.push(ratesObject);
                            tempCheckValue++;
                            break;
                        case "SEK":
                            ratesObject = {
                                currency: tempValue,
                                date: arrayReadyDate,
                                rate: rate
                            }
                            ratesFullRange.push(ratesObject);
                            tempCheckValue++;
                            break;
                    }
                    if(tempCheckValue==checkedValue.length){
                        currentArrayDate = addDays(currentArrayDate, 1);
                        arrayReadyDate = arrayedDate(currentArrayDate);
                        tempCheckValue =0;
                    }
                }
            });
            currentDate = addDays(currentDate, 1);
            console.log("(current date format)while loop after getjson: " + currentDate);
            currentParsedDate = convertDashDate(currentDate);
            setTimeout(250);
            //       await sleep(2500);
        }

        setTimeout(function () { arrayTests(); }, 500);
    }
    return false;
}
// Will remove all falsy values: undefined, null, 0, false, NaN and "" (empty string)
function cleanArray(actual) {
    var newArray = new Array();
    for (var i = 0; i < actual.length; i++) {
        if (actual[i]) {
            newArray.push(actual[i]);
        }
    }
    return newArray;
}
//+x day
function addDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
}
//fixer is a crappy page
/*function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}*/
function readyStateTest() {
    // await sleep(5000);
    if (document.readyState = "complete") {
        console.log("Document Loaded!")
        $('#currencyButton').attr("disabled", false);
        draw(ratesFullRange);
    }
}


function arrayTests() {
    console.log("Parsing Ready!");
    console.log("our complete array length ? " + ratesFullRange.length);
    for (i = 0; i < ratesFullRange.length; i++) {
        console.log(ratesFullRange[i]);
    }
    setTimeout(function () { readyStateTest(); }, 3000);
}

function convertDashDate(olddDate) {
    var convertedOldDate = new Date(olddDate);
    var month = ('0' + (convertedOldDate.getMonth() + 1)).slice(-2);
    var date = ('0' + convertedOldDate.getDate()).slice(-2);
    var year = convertedOldDate.getFullYear();
    var newdDate = year + "-" + month + "-" + date;
    return newdDate;
}
function arrayedDate(oldaDate) {
    var convertedOldDate = new Date(oldaDate);
    var month = ('0' + (convertedOldDate.getMonth() + 1)).slice(-2);
    var date = ('0' + convertedOldDate.getDate()).slice(-2);
    var year = convertedOldDate.getFullYear();
    var newaDate = date + "-" + month + "-" + year;
    return newaDate;
}