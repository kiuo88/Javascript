/**
 * Created by kiuo8 on 17.4.2017.
 */
if(window.addEventListener) {
    window.addEventListener('load', function () {
        var canvas, context;
        function init() {
            canvas = document.getElementById('imageView');
            if (!canvas) {
                alert('Error: I cannot find the canvas element!');
                return;
            }
            if (!canvas.getContext) {
                alert('Error: no canvas.getContext!');
                return;
            }
            context = canvas.getContext('2d');
            if (!context) {
                alert('Error: failed to getContext!');
                return;
            }
            tool = new tool_pencil();
            canvas.addEventListener('mousedown', ev_canvas, false);
            canvas.addEventListener('mousemove', ev_canvas, false);
            canvas.addEventListener('mouseup', ev_canvas, false);
        }
        function position ()
        {

        }
        function tool_pencil()
        {
        var tool = this;
        this.started = false;
        this.left = false;
        this.right = false;
            this.mousedown = function (ev)
            {
                if (ev.button == 0) {
                    context.beginPath();
                    context.moveTo(ev._x, ev._y);
                    tool.started = true;
                    tool.left = true;
                }
                if (ev.button == 2) {
                    context.beginPath();
                    context.moveTo(ev._x, ev._y);
                    tool.started = true;
                    tool.right = true;
                }
            };
            this.mousemove = function (ev) {
                if (tool.started) {
                    if (tool.left) {
                        context.lineWidth=1;
                        context.lineTo(ev._x, ev._y);
                        context.strokeStyle = "#000000";
                        context.stroke();
                    }
                    if (tool.right) {
                        context.lineWidth=25;
                        context.lineTo(ev._x, ev._y);
                        context.strokeStyle = "#FFFFFF";
                        context.stroke();
                    }
                }
                document.getElementById("position").textContent =  "x: " + ev._x + " & y: " + ev._y;
            };
            this.mouseup = function (ev)
            {
            if (tool.started)
                {
                tool.mousemove(ev);
                tool.started = false;
                tool.left = false;
                tool.right = false;
                }
            }
        }
        // The general-purpose event handler. This function just determines
        // the mouse position relative to the <canvas> element
        function ev_canvas (ev) {
            // Get the mouse position relative to the canvas element.
            if (ev.layerX || ev.layerX == 0) {

                // Firefox
                if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1){
                    ev._x = ev.layerX -5;
                    ev._y = ev.layerY - 91;
                }
                else { // Chrome
                        ev._x = ev.layerX;
                        ev._y = ev.layerY;
                }

            } else if (ev.offsetX || ev.offsetX == 0) { // Opera
                ev._x = ev.offsetX;
                ev._y = ev.offsetY;
            }
            var func = tool[ev.type];
            if (func)
            {
                func(ev);
            }
        }
        init();
    }, false); }